package com.csc.gdn.ipos.drive.web.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.csc.gdn.ipos.core.exception.ServiceProcessException;
import com.csc.gdn.ipos.core.profile.RequestProfile;
import com.csc.gdn.ipos.drive.api.DriveConstants;
import com.csc.gdn.ipos.drive.api.DriveKeys;
import com.csc.gdn.ipos.drive.api.audit.model.AuditData;
import com.csc.gdn.ipos.drive.api.audit.model.ProcessAuditData;
import com.csc.gdn.ipos.drive.api.audit.model.TaskAuditData;
import com.csc.gdn.ipos.drive.api.audit.model.UserAuditData;
import com.csc.gdn.ipos.drive.api.device.model.AppDevice;
import com.csc.gdn.ipos.drive.api.device.model.AppNotification;
import com.csc.gdn.ipos.drive.api.device.model.Device;
import com.csc.gdn.ipos.drive.api.model.DriveResponse;
import com.csc.gdn.ipos.drive.api.model.OldDriveResponse;
import com.google.gson.Gson;

@Controller
public class DriveController extends BaseController {

	@RequestMapping(value = "/drive/hello", method = RequestMethod.GET)
	@ResponseBody
	public OldDriveResponse hello(@RequestParam("name") String name) {
		OldDriveResponse response = new OldDriveResponse();
		response.setResult(new String("Hello " + name));
		return response;
	}
	
	/*
	 * input:
	 * @encryptedUsername
	 * @encryptedPassword
	 *  
	 * output:
	 * @drive token
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_DRIVE_SESSION, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse getSession(
			@RequestParam("encryptedUsername") String encryptedUsername,
			@RequestParam("encryptedPassword") String encryptedPassword)
			throws Exception {
		// String token = null;
		// decrypt the encrypted username, password
		String username = securityHelper.decrypt_AES(encryptedUsername);
		String password = securityHelper.decrypt_AES(encryptedPassword);

		// login to CMIS server by Apache Chemistry Client API, receive the API
		// Session object if success
		// authentication(userName, password);
		// initialize a DriveSession instance
		// generate an iPOS Drive Authentication Token using decrypted
		// username/password.
		// token = "";
		// encrypt the generated iPOS Drive Authentication Token
		// put 2 token and Apache Chemistry Session to a application/web-session
		// map (key=generatedAuthenticationToken, value=API Session instance)
		// return encryptedAuthenticationToken

		// return authentication(userName, password);

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();

		OldDriveResponse driveResponse = driveBusinessService.getDriveSession(
				requestProfile, username, password);
		/*driveResponse.setContentEncoding("result after decode: username: "
				+ username + ", " + "password: " + password);
*/
		return driveResponse;

	}
	
	@RequestMapping(value = DriveConstants.COMMAND_URL_DRIVE_SESSION_RUNTIME, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse getSessionRuntime(
			@RequestParam("encryptedUsername") String encryptedUsername,
			@RequestParam("encryptedPassword") String encryptedPassword)
			throws Exception {
		// String token = null;
		// decrypt the encrypted username, password
		String username = securityHelper.decrypt_AES(encryptedUsername);
		String password = securityHelper.decrypt_AES(encryptedPassword);

		// login to CMIS server by Apache Chemistry Client API, receive the API
		// Session object if success
		// authentication(userName, password);
		// initialize a DriveSession instance
		// generate an iPOS Drive Authentication Token using decrypted
		// username/password.
		// token = "";
		// encrypt the generated iPOS Drive Authentication Token
		// put 2 token and Apache Chemistry Session to a application/web-session
		// map (key=generatedAuthenticationToken, value=API Session instance)
		// return encryptedAuthenticationToken

		// return authentication(userName, password);

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();

		OldDriveResponse driveResponse = driveBusinessService.getDriveSessionForRuntime(requestProfile, username, password);
		/*driveResponse.setContentEncoding("result after decode: username: "
				+ username + ", " + "password: " + password);*/

		return driveResponse;

	}
	
	/*@RequestMapping(value = "/drive/session/{encryptedAuthenticationToken}", method = RequestMethod.GET)
	@ResponseBody
	public String getSession(@PathVariable String encryptedAuthenticationToken)
			throws Exception {
		return encryptedAuthenticationToken;
	}*/

	/*
	 * input:
	 * @token of drive
	 * @folder Id
	 *  
	 * output:
	 * @list folders of folder
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_DRIVE_FOLDERS, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse listFolderDocuments(
			@RequestParam("folderId") String folderId,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.listFolderFolders(
				requestProfile, authToken, folderId);

		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @folder Id
	 *  
	 * output:
	 * @list documents of folder
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_DRIVE_DOCUMENTS, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse listFolders(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("folderId") String folderId)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.listFolderDocuments(
				requestProfile, authToken, folderId);

		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @uuid
	 *  
	 * output:
	 * @content object of document(text file)
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_GET_DOCUMENT_BY_UUID, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse retrieveDocumentByUUID(
			@RequestParam("uuid") String uuid,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String token)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		String d_authToken = "";
		if (!token.isEmpty()) {
			d_authToken = token;
		}
		OldDriveResponse driveResponse = driveBusinessService
				.retrieveDocumentByUUID(requestProfile, d_authToken, uuid);

		return driveResponse;
	}

	/*@RequestMapping(value = "/drive/document/uuid/fileentry", method = { RequestMethod.POST })
	@ResponseBody
	public DriveResponse isDocumentByUUIDExist(
			@RequestParam("uuid") String uuid,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String token)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		String d_authToken = "";
		if (!token.isEmpty()) {
			d_authToken = token;
		}
		DriveResponse driveResponse = driveBusinessService
				.getDocumentFileEntryByUUID(requestProfile, d_authToken, uuid);

		return driveResponse;
	}*/

	/*@RequestMapping(value = "/drive/document/fileentry", method = { RequestMethod.POST })
	@ResponseBody
	public DriveResponse getDocumentByFileEntry(
			@RequestParam("fileentry") String fileentry,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String token)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		String d_authToken = "";
		
		if (!token.isEmpty()) {
			d_authToken = token;
		}
		DriveResponse driveResponse = driveBusinessService
				.getDocumentByFileEntry(requestProfile, d_authToken, fileentry);

		return driveResponse;
	}*/

	/*
	 * input:
	 * @token of drive
	 * @uuid
	 *  
	 * output:
	 * @content byte[] of document
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_RETRIEVE_DOCUMENT_BY_UUID, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse retrieveDocumentByUUIDBinary(
			@RequestParam("uuid") String uuid,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String token)
			throws ServiceProcessException, IOException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		String d_authToken = "";
		if (!token.isEmpty()) {
			d_authToken = token;
		}
		OldDriveResponse driveResponse = driveBusinessService
				.retrieveDocumentByUUIDBinary(requestProfile, d_authToken, uuid);

		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @parentFolderId
	 *  
	 * output:
	 * @Json array of fileEntryId
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_REMOVE_ALL_CHILD_DOCUMENT, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse removeAllDocument(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("parentFolderId") String parentFolderId)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.removeAllDocument(
				requestProfile, authToken, parentFolderId);

		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @parentFolderId
	 *  
	 * output:
	 * @Json array of folderId
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_REMOVE_ALL_CHILD_FOLDER, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse removeAllFolder(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("parentFolderId") String parentFolderId)
			throws ServiceProcessException {
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.removeAllFolder(
				requestProfile, authToken, parentFolderId);

		return driveResponse;
	}
	
	/*
	 * input:
	 * @token of drive
	 * @folderId
	 *  
	 * output:
	 * @folderId
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_REMOVE_FOLDER, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse removeFolder(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("folderId") String folderId)
					throws ServiceProcessException {
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.removeFolder(requestProfile, authToken, folderId);
		
		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @fileEntryId
	 *  
	 * output:
	 * @content byte[] of document
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_GET_DOCUMENT_BY_FILE_ENTRY_ID, method = { RequestMethod.POST })
	@ResponseBody
	public byte[] retrieveDocument(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("fileEntryId") String fileEntryId)
			throws ServiceProcessException, IOException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService
				.retrieveDocumentByFileEntryId(requestProfile, authToken,
						fileEntryId);

		return (byte[]) driveResponse.getResult();
	}

	/*
	 * input:
	 * @token of drive
	 * @file
	 * @parentFolderId
	 * @description
	 * @changeLog
	 *  
	 * output:
	 * @file entry
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_SAVE_DOCUMENT, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse saveDocuments(
			@RequestParam("file") MultipartFile file,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("parentFolderId") String parentFolderId,
			@RequestParam("description") String description,
			@RequestParam("changeLog") String changeLog) throws Exception {
//		String content = file.getName();
//		System.out.println(content);

		File tempFile = multipartToFile(file);
		/*InputStream is = new FileInputStream(tempFile);
		int size = is.available();

		for (int i = 0; i < size; i++) {
			System.out.print((char) is.read());
		}
		is.close();*/

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.saveDocument(requestProfile, authToken, parentFolderId, description, changeLog, tempFile);

		tempFile.delete();
		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @file
	 * @parentFolderId
	 * @description
	 * @changeLog
	 * @jsonMetaData
	 * @approved
	 *  
	 * output:
	 * @file entry
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_SAVE_DOCUMENT_META, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse saveDocumentWithMetaData(
			@RequestParam("file") MultipartFile file,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("parentFolderId") String parentFolderId,
			@RequestParam("description") String description,
			@RequestParam("changeLog") String changeLog,
			@RequestParam("jsonMetaData") String jsonMetaData,
			@RequestParam("approved") String approved) throws Exception {
		File tempFile = multipartToFile(file);
		/*
		 * String jsonMetaData = "";
		 * 
		 * Map<String, String> metadataMap = new HashMap<String, String>();
		 * metadataMap.put("metaFirstName","meta-First-Name");
		 * metadataMap.put("metaLastName","meta-Last-Name");
		 * 
		 * Gson gson = new Gson(); jsonMetaData = gson.toJson(metadataMap);
		 */

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService
				.saveDocumentWithMetaData(requestProfile, authToken,
						parentFolderId, description, changeLog, jsonMetaData,
						Boolean.valueOf(approved), tempFile);

		tempFile.delete();
		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @file
	 * @uuid
	 * @description
	 * @changeLog
	 * @majorVersion
	 * @jsonMetaData
	 *  
	 * output:
	 * @file entry
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_UPDATE_DOCUMENT_META, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse updateDocumentWithMetaData(
			@RequestParam("file") MultipartFile file,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("uuid") String uuid,
			@RequestParam("description") String description,
			@RequestParam("changeLog") String changeLog,
			@RequestParam("majorVersion") String majorVersion,
			@RequestParam("fileTypeId") String fileTypeId,
			@RequestParam("jsonMetaData") String jsonMetaData) throws Exception {
		File tempFile = multipartToFile(file);

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();

		OldDriveResponse driveResponse = driveBusinessService.updateDocumentWithMetaData(requestProfile, authToken, uuid, description, changeLog, Boolean.valueOf(majorVersion),fileTypeId, jsonMetaData, tempFile);
		tempFile.delete();
		return driveResponse;
	}

	
	/*
	 * input:
	 * @token of drive
	 * @fileEntryId
	 * @destFolderId
	 *  
	 * output:
	 * @file entry
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_COPY_DOCUMENT, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse copyDocument(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("fileEntryId") String fileEntryId,
			@RequestParam("destFolderId") String destFolderId)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.copyDocument(
				requestProfile, authToken, fileEntryId, destFolderId);

		return driveResponse;
	}


	/*
	 * input:
	 * @token of drive
	 * @keywords(map)
	 *  
	 * output:
	 * @list meta data
	 */
	@RequestMapping(value = DriveConstants.COMMAND_URL_GET_METADATA, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse getDocumentMetaData(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("keywords") String keywords)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		Gson gson = new Gson();
		@SuppressWarnings("unchecked")
		Map<String, String> map = gson.fromJson(keywords, Map.class);
		OldDriveResponse driveResponse = driveBusinessService.getDocumentMetaData(
				requestProfile, authToken, map);

		return driveResponse;
	}
	
	/*
	 * input:
	 * @token of drive
	 * @sourceFolderId
	 * @parentFolderId
	 * @name
	 * @description
	 *  
	 * output:
	 * @folder entry
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_COPY_FOLDER, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse copyFolder(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("sourceFolderId") String sourceFolderId,
			@RequestParam("parentFolderId") String parentFolderId,
			@RequestParam("name") String name,
			@RequestParam("description") String description)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.copyFolder(
				requestProfile, authToken, sourceFolderId, parentFolderId,
				name, description);

		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @path
	 * @fileName
	 *  
	 * output:
	 * @list meta data
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_GET_METADATA_BY_PATH, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse getMetaDataByPath(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("path") String path,
			@RequestParam("fileName") String fileName)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService
				.getDocumentMetaDataByPath(requestProfile, authToken, path,
						fileName);

		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @name
	 * @description
	 * @listFields(list)
	 *  
	 * output:
	 * @document type entry
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_CREATE_METADATA, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse  createMetaData(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("name") String name,  
			@RequestParam("description") String description,  
			@RequestParam("listFields") String listFields) throws ServiceProcessException {
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.createDocumentMetaData(requestProfile, authToken, name, description, listFields);
		
		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @file
	 * @path
	 * @description
	 * @changeLog
	 *  
	 * output:
	 * @file entry
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_SAVE_DOCUMENT_BY_PATH, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse  saveDocumentByPath(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("path") String path, 
			@RequestParam("description") String description,
			@RequestParam("changeLog") String changeLog,
			@RequestParam("file") MultipartFile file) throws Exception {

//		String content = file.getName();
//		System.out.println(content);

		File tempFile = multipartToFile(file);
		/*
		InputStream is = new FileInputStream(tempFile);
		int size = is.available();

		for (int i = 0; i < size; i++) {
			System.out.print((char) is.read());
		}
		is.close();
		*/
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.saveDocumentByPath(requestProfile, authToken, path, description, changeLog, tempFile);
		
		tempFile.delete();
		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @file
	 * @path
	 * @description
	 * @changeLog
	 * @jsonMetaData
	 * @approved
	 *  
	 * output:
	 * @file entry
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_SAVE_DOCUMENT_META_BY_PATH, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse saveDocumentWithMetaDataByPath(
			@RequestParam("file") MultipartFile file,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("path") String path,
			@RequestParam("description") String description,
			@RequestParam("changeLog") String changeLog,
			@RequestParam("fileTypeId") String fileTypeId,
			@RequestParam("jsonMetaData") String jsonMetaData,
			@RequestParam("approved") String approved) throws Exception {		
		File tempFile = multipartToFile(file);		
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.saveDocumentWithMetaDataByPath(requestProfile, authToken, path, description, changeLog, fileTypeId, jsonMetaData, Boolean.valueOf(approved), tempFile);
		
		tempFile.delete();
		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @parentFolderId
	 * @folderName
	 *  
	 * output:
	 * @file entry
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_CHECK_FOLDER_EXIST_BY_NAME, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse checkFolderExistByName(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("parentFolderId") String parentFolderId,
			@RequestParam("folderName") String folderName) throws Exception {
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService
				.checkFolderExistByName(requestProfile, authToken,
						parentFolderId, folderName);

		return driveResponse;
	}
	
	/*
	 * input:
	 * @token of drive
	 * @parentFolderId
	 * @folderName
	 *  
	 * output:
	 * @folder entry
	 */
	@RequestMapping(value = DriveConstants.COMMAND_URL_ADD_FOLDER, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse addFolder(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("parentFolderId") String parentFolderId,
			@RequestParam("folderName") String folderName) throws Exception {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService
				.addFolder(requestProfile, authToken,
						parentFolderId, folderName);

		return driveResponse;
	}
	
	/*
	 * input:
	 * @token of drive
	 * @parentFolderId
	 * @fileName
	 *  
	 * output:
	 * @file entry
	 */
	@RequestMapping(value = DriveConstants.COMMAND_URL_REMOVE_DOCUMENT_BY_FOLDERID_AND_DOCUMENT_NAME, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse removeDocumentByFolderIdAndDocumentName(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("parentFolderId") String parentFolderId,
			@RequestParam("fileName") String fileName) throws Exception {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService
				.removeFileByFolderIdAndFileName(requestProfile, authToken, parentFolderId, fileName);

		return driveResponse;
	}
	
	/*
	 * input:
	 * @token of drive
	 * @parentFolderId
	 * @fileName
	 *  
	 * output:
	 * @content byte[] of document
	 */
	@RequestMapping(value = DriveConstants.COMMAND_URL_RETRIVE_DOCUMENT_BY_FOLDERID_AND_DOCUMENT_NAME, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse retriveDocumentByFolderIdAndDocumentName(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("parentFolderId") String parentFolderId,
			@RequestParam("fileName") String fileName) throws Exception {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.retrieveDocumentByFolderIdAndFileName(requestProfile, authToken, parentFolderId, fileName);

		return driveResponse;
	}
	
	/*
	 * input:
	 * @token of drive
	 *  
	 * output:
	 * @list all document of DMS
	 */
	@RequestMapping(value = DriveConstants.COMMAND_URL_GET_ALL_DOCUMENT, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse getAllDocument(@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken) throws Exception {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.getAllDocument(requestProfile, authToken);

		return driveResponse;
	}
	
	/*This funtion is test for using another DMS*/
	@RequestMapping(value = DriveConstants.COMMAND_URL_EXAMPLE_SECOND_DMS, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse exampleSecondDMS(@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken) throws Exception {
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.exampleSecondDMS(requestProfile, authToken);
		
		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @uuid
	 *  
	 * output:
	 * @content object and meta data of document 
	 */
	@RequestMapping(value = DriveConstants.COMMAND_URL_RETRIEVE_DOCUMENT_WITH_META_DATA, method = { RequestMethod.POST })
	@ResponseBody
	public OldDriveResponse retrieveDocumentWithMetaData(
			@RequestParam("uuid") String uuid,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String token)
			throws ServiceProcessException {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.retrieveDocumentWithMetaData(requestProfile, token, uuid);

		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @name
	 * @description
	 * @listFields(list)
	 *  
	 * output:
	 * @document type entry 
	 */
	@RequestMapping(value = DriveConstants.COMMAND_URL_UPDATE_METADATA, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse updateDocumentType(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("name") String name,
			@RequestParam("description") String description,
			@RequestParam("listFields") String listFields) throws ServiceProcessException{
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.updateDocumentMetaData(requestProfile, authToken, name, description, listFields);
		return driveResponse;
	}
	
	@RequestMapping(value = DriveConstants.COMMAND_URL_CHECK_FILE_EXIST_BY_NAME, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse checkDocumentExistByName(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("parentFolderId") String parentFolderId,
			@RequestParam("title") String title) throws Exception {
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService
				.checkDocumentExistByName(requestProfile, authToken, parentFolderId, title);

		return driveResponse;
	
	}
	/*	 * input:
	 * @token of drive
	 *  
	 * output:
	 * @list all document of DMS
	 */
	@RequestMapping(value = DriveConstants.COMMAND_URL_GET_ALL_DOCUMENT_OF_FOLDER_BY_PATH, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse getAllDocumentOfFolderByPath(@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken, @RequestParam("path") String path) throws Exception {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.getAllDocumentOfFolderByPath(requestProfile, authToken, path);

		return driveResponse;
	}

	/*
	 * input:
	 * @token of drive
	 * @listUUID
	 * 
	 * output
	 * @list all metadata
	 * */
	@RequestMapping(value = DriveConstants.COMMAND_URL_GET_METADATAS_BY_LIST_UUID, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse getMetaDatasByListUuid(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("listUuid") String listUuid) throws Exception{
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.getMetaDatasByListUuid(requestProfile, authToken, listUuid) ;
		return driveResponse;
	}
	
	/*
	 * input:
	 * @token of drive
	 * @uuid
	 * 
	 * output
	 * @list all version
	 * */
	@RequestMapping(value = DriveConstants.COMMAND_URL_GET_ALL_DOCUMENT_VERSION, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse getAllDocumentVersion(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("uuid") String uuid) throws Exception{
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.getAllDocumentVersion(requestProfile, authToken, uuid);
		return driveResponse;
	}
	
	/*
	 * input:
	 * @token of drive
	 * @uuid
	 *  
	 * output:
	 * @fileEntryId
	 */
	@RequestMapping(value = DriveConstants.COMMAND_URL_REMOVE_DOCUMENT_BY_UUID, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse removeDocumentByUUID(@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken, @RequestParam("uuid") String uuid) throws Exception {
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.removeDocumentByUUID(requestProfile, authToken, uuid);
		
		return driveResponse;
	}
	
	/*test insert into process table*/
	@RequestMapping(value = DriveConstants.COMMAND_URL_INSERT_PROCESS_AUDIT_DATA, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse insertProcessAuditData(@RequestParam("data") String data) throws Exception {
		List<ProcessAuditData> listProcess = new ArrayList<ProcessAuditData>();
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		
		for(int i=0; i<1; i++) {
//			String primaryKey = UUID.randomUUID().toString();
			
			ProcessAuditData processAuditData = new ProcessAuditData();
			processAuditData.setProcessId(data);
			processAuditData.setAuditId(data);
			processAuditData.setProcessName(data);
			processAuditData.setStatus(data);
			processAuditData.setStartAt("2015-0"+ (i+1) +"-0"+ (i+1) +" 0"+ (i+1) +":0"+ (i+1) +":0"+ (i+1));
			processAuditData.setDuration(data);
			processAuditData.setErrorMsg(data);
			processAuditData.setAuditTime("2015-0"+ (i+1) +"-0"+ (i+1) +" 0"+ (i+1) +":0"+ (i+1) +":0"+ (i+1));
			
			listProcess.add(processAuditData);
		}
		OldDriveResponse driveResponse = driveBusinessService.insertProcessAuditData(requestProfile, listProcess);
		
		return driveResponse;
	}
	
	/*test insert into process table*/
	@RequestMapping(value = DriveConstants.COMMAND_URL_INSERT_TASK_AUDIT_DATA, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse insertTaskAuditData(@RequestParam("data") String data) throws Exception {
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		List<TaskAuditData> listTask = new ArrayList<TaskAuditData>();
		for(int i=0; i<2; i++) {	
			String primaryKey = UUID.randomUUID().toString();
			
			TaskAuditData taskAuditData = new TaskAuditData();
			taskAuditData.setProcessId(primaryKey);
			taskAuditData.setStatus(data);
			taskAuditData.setTaskName(data);
			taskAuditData.setStartAt("2015-0"+ (i+1) +"-0"+ (i+1) +" 0"+ (i+1) +":0"+ (i+1) +":0"+ (i+1));
			taskAuditData.setDuration(data);
			taskAuditData.setException(data);
			taskAuditData.setAuditTime("2015-0"+ (i+1) +"-0"+ (i+1) +" 0"+ (i+1) +":0"+ (i+1) +":0"+ (i+1));
			
			listTask.add(taskAuditData);
		}
		OldDriveResponse driveResponse = driveBusinessService.insertTaskAuditData(requestProfile, listTask);
		
		return driveResponse;
	}
	
	/*test insert into process table*/
	@RequestMapping(value = DriveConstants.COMMAND_URL_INSERT_USER_AUDIT_DATA, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse insertUserAuditData(@RequestParam("data") String data) throws Exception {
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		List<UserAuditData> listUser = new ArrayList<UserAuditData>();
		
		for(int i=0; i<2; i++) {
			String primaryKey = UUID.randomUUID().toString();
			
			UserAuditData userAuditData = new UserAuditData();
			userAuditData.setAuditId(primaryKey);
			userAuditData.setTransactionId(data);
			userAuditData.setStatus(data);
			userAuditData.setDocId(data);
			userAuditData.setOldVersion(data);
			userAuditData.setNewVersion(data);
			userAuditData.setDocType(data);
			userAuditData.setAction(data);
			userAuditData.setActionBy(data);
			userAuditData.setStartAt("2015-0"+ (i+1) +"-0"+ (i+1) +" 0"+ (i+1) +":0"+ (i+1) +":0"+ (i+1));
			userAuditData.setDuration(data);
			userAuditData.setRequest(data);
			userAuditData.setResponse(data);
			userAuditData.setContextPath(data);
			userAuditData.setErrorMsg(data);
			userAuditData.setHitCache("0");
			userAuditData.setPlatform(data);
			userAuditData.setDevice(data);
			userAuditData.setAuditTime("2015-0"+ (i+1) +"-0"+ (i+1) +" 0"+ (i+1) +":0"+ (i+1) +":0"+ (i+1));
			listUser.add(userAuditData);
		}
		
		OldDriveResponse driveResponse = driveBusinessService.insertUserAuditData(requestProfile, listUser);
		
		return driveResponse;
	}
	
	/*test insert into process table*/
	@RequestMapping(value = "/drive/select/all/process/audit", method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse selectAllProcessAuditData() throws Exception {
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.selectAllProcessAuditData(requestProfile);
		
		return driveResponse;
	}
	
	/*
	 * input:
	 * @token of drive
	 * @file
	 * @uuid
	 * @description
	 * @changeLog
	 * @majorVersion
	 *  
	 * output:
	 * @file entry
	 */	
	@RequestMapping(value = DriveConstants.COMMAND_URL_UPDATE_DOCUMENT, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse updateDocument(
			@RequestParam("file") MultipartFile file,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("uuid") String uuid,
			@RequestParam("description") String description,
			@RequestParam("changeLog") String changeLog,
			@RequestParam("majorVersion") String majorVersion) throws Exception {
		File tempFile = multipartToFile(file);

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();

		OldDriveResponse driveResponse = driveBusinessService.updateDocument(requestProfile, authToken, uuid, description, changeLog, Boolean.valueOf(majorVersion), tempFile);

		tempFile.delete();
		return driveResponse;
	}
	
	/*
	 * input:
	 * @token of drive
	 * @uuid
	 * @file version
	 * 
	 * output
	 * @document in byte[]
	 * */
	@RequestMapping(value = DriveConstants.COMMAND_URL_RETRIEVE_DOCUMENT_BY_UUID_AND_VERSION, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse retriveDocumentByUuidAndVersion(
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken,
			@RequestParam("uuid") String uuid,
			@RequestParam("version") String version) throws Exception{
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.retrieveDocumentByUuidAndVersion(requestProfile, authToken, uuid, version);
		return driveResponse;
	}

	/*insert into database audit*/
	@RequestMapping(value = DriveConstants.COMMAND_URL_INSERT_AUDIT_DATA, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse insertAuditData(@RequestParam("data") String data) throws Exception {
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		AuditData auditData = null; 
		auditData = extractAuditData(data); 
		
		OldDriveResponse driveResponse = driveBusinessService.insertAuditData(requestProfile, auditData);
		
		return driveResponse;
	}
	
	@RequestMapping(value = DriveConstants.COMMAND_URL_DELETE_DOCUMENT_TYPE, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse deleteDocumentType(
			@RequestParam("fileTypeId") String fileTypeId,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken) throws Exception {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.deleteDocumentType(requestProfile, authToken, fileTypeId);

		return driveResponse;
	}
	
	/*insert into database portal*/
	@RequestMapping(value = "/drive/insert/user/portal", method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse insertUserData(@RequestParam("data") String data) throws Exception {
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.insertUserData(requestProfile, data);
		
		return driveResponse;
	}
	
	/*logout device-delete data in portal database*/
	@RequestMapping(value = "/drive/delete/user/portal", method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse deleteUserData(@RequestParam("data") String data) throws Exception {
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.deleteUserData(requestProfile, data);
		
		return driveResponse;
	}

	
	@RequestMapping(value = DriveConstants.COMMAND_URL_SAVE_DEVICE, method = RequestMethod.POST)
	@ResponseBody
	public DriveResponse saveDeviceData(@RequestParam("deviceInfo") String deviceInfo) {
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		Device device = new Device();
		DriveResponse driveResponse = new DriveResponse();
		try {
			device = extractDevicesData(deviceInfo);
			driveResponse = driveBusinessService.insertDeviceData(requestProfile, device);
		} catch (Exception e) {
			driveResponse.setError_code(1);

			driveResponse.setError_message(e.getMessage());
		}
		
		return driveResponse;
	}
	
	@RequestMapping(value = DriveConstants.COMMAND_URL_SAVE_APP_DEVICE, method = RequestMethod.POST)
	@ResponseBody
	public DriveResponse saveAppDeviceData(@RequestParam("deviceInfo") String deviceInfo) {
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		Device device = new Device();
		AppDevice appDevice = new AppDevice();
		DriveResponse driveResponse = new DriveResponse();
		try {
			device = extractDevicesData(deviceInfo);
			appDevice = extractAppDevicesData(deviceInfo);
			driveResponse = driveBusinessService.insertAppDeviceData(requestProfile, device, appDevice);
		} catch (Exception e) {
			driveResponse.setError_code(1);

			driveResponse.setError_message(e.getMessage());
		}
		
		return driveResponse;
	}
	
	@RequestMapping(value = DriveConstants.COMMAND_URL_SAVE_NOTIFICATION, method = RequestMethod.POST)
	@ResponseBody
	public DriveResponse saveNotification(@RequestParam("deviceToken") String deviceToken, @RequestParam("deviceInfo") String deviceInfo, @RequestParam("userId") String userId) throws Exception {
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();

		DriveResponse driveResponse = new DriveResponse();
		try {
			saveDeviceData(deviceInfo);
			DriveResponse response = saveAppDeviceData(deviceInfo);
			@SuppressWarnings("rawtypes")
			Map map = (Map)response.getData();
			AppDevice appDevice = (AppDevice)map.get(DriveKeys.APP_DEVICE_DATA);
			AppNotification appNotifications = extractAppNotificationsData(deviceToken, userId, appDevice.getAppDeviceId());			
			driveResponse = driveBusinessService.insertNotification(requestProfile, appNotifications);
		} catch (Exception e) {
			driveResponse.setError_code(1);
			driveResponse.setError_message(e.toString());
		}
		
		return driveResponse;
	}	
	
	@RequestMapping(value = DriveConstants.COMMAND_URL_MOVE_DOCUMENT, method = RequestMethod.POST)
	@ResponseBody
	public OldDriveResponse moveFileEntry(
			@RequestParam("path") String path,
			@RequestParam("fileName") String fileName,
			@RequestParam("targetFolderId") String targetFolderId,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken) throws Exception {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		OldDriveResponse driveResponse = driveBusinessService.moveFileEntry(requestProfile, authToken, path, fileName, targetFolderId);

		return driveResponse;
	}
	
	@RequestMapping(value = DriveConstants.COMMAND_URL_GET_LATEST_VERSION, method = RequestMethod.POST)
	@ResponseBody
	public DriveResponse getLatestVersion(
			@RequestParam("uuid") String uuid,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken) throws Exception {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		DriveResponse driveResponse = driveBusinessService.getLatestVersion(requestProfile, authToken, uuid);

		return driveResponse;
	}
	
	@RequestMapping(value = DriveConstants.COMMAND_URL_MOVE_FOLDER, method = RequestMethod.POST)
	@ResponseBody
	public DriveResponse moveFolder(
			@RequestParam("folderId") String folderId,
			@RequestParam("targetParentFolderId") String targetParentFolderId,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken) throws Exception {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		DriveResponse driveResponse = driveBusinessService.moveFolder(requestProfile, authToken, folderId, targetParentFolderId);

		return driveResponse;
	}
	
	@RequestMapping(value = DriveConstants.COMMAND_URL_GET_METADATA_BY_VERSION, method = RequestMethod.POST)
	@ResponseBody
	public DriveResponse getMetaDataByVersion(
			@RequestParam("uuid") String uuid,
			@RequestParam("version") String version,
			@RequestHeader(DriveConstants.HEADER_SESSION_TOKEN) String authToken) throws Exception {

		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		DriveResponse driveResponse = driveBusinessService.getMetaDataByVersion(requestProfile, authToken, uuid, version);

		return driveResponse;
	}
	
	@RequestMapping(value = DriveConstants.COMMAND_URL_REMOVE_NOTIFICATION, method = RequestMethod.POST)
	@ResponseBody
	public DriveResponse removeNotification(@RequestParam("deviceToken") String deviceToken) {
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		DriveResponse driveResponse = new DriveResponse();
		AppNotification appNotification = new AppNotification();
		try {			
			appNotification.setDeviceToken(deviceToken);			
			driveResponse = driveBusinessService.removeNotification(requestProfile, appNotification);
		} catch (Exception e) {
			driveResponse.setError_code(1);
			driveResponse.setError_message(e.toString());
		}
		
		return driveResponse;
	}	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/drive/upload/files", method = RequestMethod.POST)
	@ResponseBody
	public DriveResponse uploadMultiFile(@RequestParam("files") MultipartFile[] multipartFile) {
		MultipartFile multipartFile1 = multipartFile[0];
		MultipartFile multipartFile2 = multipartFile[1];
		DriveResponse driveResponse = new DriveResponse();
		Map map = new HashMap<String, String>();
		map.put("result", "1."+multipartFile1.getOriginalFilename()+" - 2."+multipartFile2.getOriginalFilename());
		driveResponse.setData(map);
		return driveResponse;
	}	
}