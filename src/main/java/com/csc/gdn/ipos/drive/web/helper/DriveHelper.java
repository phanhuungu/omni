package com.csc.gdn.ipos.drive.web.helper;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.BasicHttpContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.csc.gdn.ipos.core.profile.RequestProfile;
import com.csc.gdn.ipos.core.service.message.MessageHelper;
import com.csc.gdn.ipos.drive.storage.model.DriveStorageDataSource;

public class DriveHelper {	
	@Autowired
	private DriveStorageDataSource driveStorageDataSource;
//	private DMSServerConfig iposDriveDataSource;
	private MessageHelper messageHelper;
//	private static Map<String,String> authenInfo;
//	private static Map<String, List<Object>> authenData;
	
//	public DMSServerConfig getIposDriveDataSource() {
//		return iposDriveDataSource;
//	}
//
//	public void setIposDriveDataSource(DMSServerConfig iposDriveDataSource) {
//		this.iposDriveDataSource = iposDriveDataSource;
//	}

	public MessageHelper getMessageHelper() {
		return messageHelper;
	}

	public void setMessageHelper(MessageHelper messageHelper) {
		this.messageHelper = messageHelper;
	}

	public URIBuilder defaultUriBuilder() {
		return new URIBuilder().setScheme("http").setHost(driveStorageDataSource.getHost()).setPort(driveStorageDataSource.getPort());
	}
	
/*	public HttpResponseWrapper doRequest(HttpRequest request, Message message)
			throws Exception {
		String userName;
		String password;
		DriveStorageProfile driveStorageProfile = (DriveStorageProfile)messageHelper.getComponentProfile(message);		
		DriveRequest driveRequest = (DriveRequest)driveStorageProfile.getInput().get(DriveInputKey.DRIVE_REQUEST);

		userName = driveRequest.getUserName();
		password = driveRequest.getPassword();
		CloseableHttpClient httpclient;;
		HttpHost targetHost;
		BasicHttpContext ctx;
		
		List<Object> listAuthenData = getAuthenData(userName, password);
		httpclient = (CloseableHttpClient) listAuthenData.get(0);
		targetHost = (HttpHost) listAuthenData.get(1);
		ctx= (BasicHttpContext) listAuthenData.get(2);
		
		HttpResponse resp = httpclient.execute(targetHost, request, ctx);
		HttpEntity entity = resp.getEntity();
		
		HttpResponseWrapper responseWrapper = new HttpResponseWrapper();
		
		if (entity != null) {
			byte[] byteResult = IOUtils.toByteArray(entity.getContent());
			if(isLoginFail(byteResult)) {
				System.out.println("Login fail!");
			}
			else{
				
				addDataAuthen(userName, password, httpclient, targetHost, ctx);				
			}

			responseWrapper.setContent(byteResult);
			Header contentEncoding = entity.getContentEncoding();
			Header contentType = entity.getContentType();
			if (contentEncoding != null) {
				responseWrapper.setContentEncoding(contentEncoding.getValue());
			}
			if (contentType != null) {
				responseWrapper.setContentType(contentType.getValue());
			}
		}
		return responseWrapper;
	}
*/

	public HttpHost getTargetHost()
			throws Exception {
		HttpHost targetHost = new HttpHost(driveStorageDataSource.getHost(), driveStorageDataSource.getPort(), "http");
		return targetHost;
	}
	
	public CloseableHttpClient getHttpClient(String userName, String password) {
		CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(AuthScope.ANY,
				new UsernamePasswordCredentials(userName, password));
		CloseableHttpClient httpclient = HttpClientBuilder.create()
				.setDefaultCredentialsProvider(credentialsProvider).build();
		
		return httpclient;
	}
	
	public BasicHttpContext getBasicHttpContext(HttpHost targetHost) {
		AuthCache authCache = new BasicAuthCache();
		BasicScheme basicAuth = new BasicScheme();
		authCache.put(targetHost, basicAuth);

		// Add AuthCache to the execution context
		BasicHttpContext ctx = new BasicHttpContext();
		ctx.setAttribute(HttpClientContext.AUTH_CACHE, authCache);
		
		return ctx;
	}
/*	
	private boolean isLoginFail(byte[] byteResult) throws Exception{
		String authenticationFail = "{\"exception\":\"Authenticated access required\"}";
		byte[] byteMessage = authenticationFail.getBytes();
		if(Arrays.equals(byteMessage, byteResult))
		{
			return true;
		} else{				
			return false;
		}

	}
	
	private void addDataAuthen(String userName, String password, CloseableHttpClient httpclient, HttpHost targetHost, BasicHttpContext ctx) {
		if(authenInfo == null){
			authenInfo = new HashMap<String, String>();
		}
		if(authenData == null){
			authenData = new HashMap<String, List<Object>>();
		}
		if(!password.equalsIgnoreCase(authenInfo.get(userName))){
			authenInfo.put(userName, password);		
			List<Object> dataAuthen = new ArrayList<Object>();
			dataAuthen.add(httpclient);
			dataAuthen.add(targetHost);
			dataAuthen.add(ctx);
			if(authenData.get(userName) != null)
			{
				authenData.remove(userName);
			}
			authenData.put(userName, dataAuthen);
		}
	}
	
	private List<Object> getAuthenData(String userName, String password) throws Exception {
		List<Object> listAuthenData = new ArrayList<Object>();
		CloseableHttpClient httpclient = null;;
		HttpHost targetHost = null;
		BasicHttpContext ctx = null;
		if(authenInfo != null) {
			if(!authenInfo.get(userName).equalsIgnoreCase(password)){
				httpclient = getHttpClient(userName, password);
				targetHost = getTargetHost();
				ctx= getBasicHttpContext(targetHost);
			}else{
				httpclient = (CloseableHttpClient) authenData.get(userName).get(0);
				targetHost = (HttpHost)authenData.get(userName).get(1);
				ctx = (BasicHttpContext) authenData.get(userName).get(2);
			}	
		}else {
			httpclient = getHttpClient(userName, password);
			targetHost = getTargetHost();
			ctx= getBasicHttpContext(targetHost);
		}
		listAuthenData.add(httpclient);
		listAuthenData.add(targetHost);
		listAuthenData.add(ctx);
		
		return listAuthenData;
	}

	
	public String getFileEntryByUUID(String groupId, String uuid, Message message)
			throws Exception {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("uuid", uuid));
		params.add(new BasicNameValuePair("groupId", groupId));

		URI uri = defaultUriBuilder()
				.setPath("/api/jsonws/dlapp/get-file-entry-by-uuid-and-group-id")
				.setParameters(params).build();
		HttpGet request = new HttpGet(uri);
		HttpResponseWrapper responseWrapper = doRequest(request, message);
		String result = null;
		if (responseWrapper.getContent() != null) {
			result = new String(responseWrapper.getContent());
		}
		return result;
	}

	public String getFolderEntryById(String folderId, Message message)
			throws Exception {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("folderId", folderId));

		URI uri = defaultUriBuilder()
				.setPath("/api/jsonws/dlfolder/get-folder")
				.setParameters(params).build();
		HttpGet request = new HttpGet(uri);
		HttpResponseWrapper responseWrapper = doRequest(request, message);
		String result = null;
		if (responseWrapper.getContent() != null) {
			result = new String(responseWrapper.getContent());
		}
		return result;
	}
	
	public String getFileEntryByFileEntryID(String fileEntryId, Message message)
			throws Exception {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("fileEntryId", fileEntryId));

		URI uri = defaultUriBuilder()
				.setPath("/api/jsonws/dlapp/get-file-entry")
				.setParameters(params).build();
		HttpGet request = new HttpGet(uri);
		HttpResponseWrapper responseWrapper = doRequest(request, message);
		String result = null;
		if (responseWrapper.getContent() != null) {
			result = new String(responseWrapper.getContent());
		}
		return result;
	}
	
	public JSONArray getAllChildFolder(String repositoryId, String parentFolderId, Message message)
			throws Exception {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("repositoryId", repositoryId));
		params.add(new BasicNameValuePair("parentFolderId", parentFolderId));

		URI uri = defaultUriBuilder()
				.setPath("/api/jsonws/dlapp/get-folders")
				.setParameters(params).build();
		HttpGet request = new HttpGet(uri);

		HttpResponseWrapper responseWrapper = doRequest(request, message);
		String result = null;
		if (responseWrapper.getContent() != null) {
			result = new String(responseWrapper.getContent());
		}
		Object obj = JSONValue.parse(result);
		JSONArray array = (JSONArray) obj;
		
		return array;
	}
	
	public JSONArray getAllChildDocument(String repositoryId, String folderId, Message message)
			throws Exception {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("repositoryId", repositoryId));
		params.add(new BasicNameValuePair("folderId", folderId));

		URI uri = defaultUriBuilder()
				.setPath("/api/jsonws/dlapp/get-file-entries")
				.setParameters(params).build();
		HttpGet request = new HttpGet(uri);

		HttpResponseWrapper responseWrapper = doRequest(request, message);
		String result = null;
		if (responseWrapper.getContent() != null) {
			result = new String(responseWrapper.getContent());
		}
		Object obj = JSONValue.parse(result);
		JSONArray array = (JSONArray) obj;
		
		return array;
	}
	*/
	
	
	public RequestProfile getEmptyRequestProfile(){
		return new RequestProfile("", "", "", "", null, null, null, null);
	}
	
	
	
	
	
	
	
	
	
}
