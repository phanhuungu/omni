package com.csc.gdn.ipos.drive.web.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.web.multipart.MultipartFile;

import com.csc.gdn.ipos.core.common.GeneratorHelper;
import com.csc.gdn.ipos.drive.api.audit.model.AuditData;
import com.csc.gdn.ipos.drive.api.audit.model.ProcessAuditData;
import com.csc.gdn.ipos.drive.api.audit.model.TaskAuditData;
import com.csc.gdn.ipos.drive.api.audit.model.UserAuditData;
import com.csc.gdn.ipos.drive.api.device.model.AppDevice;
import com.csc.gdn.ipos.drive.api.device.model.AppNotification;
import com.csc.gdn.ipos.drive.api.device.model.Device;
import com.csc.gdn.ipos.drive.api.impl.helper.SecurityHelper;
import com.csc.gdn.ipos.drive.web.bsi.IBusinessServiceInterface;
import com.csc.gdn.ipos.drive.web.helper.DriveHelper;

public class BaseController {

	@Resource
	protected IBusinessServiceInterface driveBusinessService;
	@Resource
	protected SecurityHelper securityHelper;
	@Resource
	protected GeneratorHelper generatorHelper;
	@Resource
	protected DriveHelper driveHelper;

	public BaseController() {
	}

	public SecurityHelper getSecurityHelper() {
		return securityHelper;
	}

	public void setSecurityHelper(SecurityHelper securityHelper) {
		this.securityHelper = securityHelper;
	}

	public GeneratorHelper getGeneratorHelper() {
		return generatorHelper;
	}

	public void setGeneratorHelper(GeneratorHelper generatorHelper) {
		this.generatorHelper = generatorHelper;
	}

	public DriveHelper getDriveHelper() {
		return driveHelper;
	}

	public void setDriveHelper(DriveHelper driveHelper) {
		this.driveHelper = driveHelper;
	}

	public static byte[] toByteArray(Object obj) throws IOException {
		byte[] bytes = null;
		ByteArrayOutputStream bos = null;
		ObjectOutputStream oos = null;
		try {
			bos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(bos);
			oos.writeObject(obj);
			oos.flush();
			bytes = bos.toByteArray();
		} finally {
			if (oos != null) {
				oos.close();
			}
			if (bos != null) {
				bos.close();
			}
		}
		return bytes;
	}

	public File multipartToFile(MultipartFile multipart)
			throws IllegalStateException, IOException {
		String tDir = System.getProperty("java.io.tmpdir");
		File convFile = new File(tDir + "\\" + multipart.getOriginalFilename());
		multipart.transferTo(convFile);
		return convFile;
	}

	public File convertMultipartToFile(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	/* This function converts an object to array of bytes */
	public static byte[] convertToBytes(Object obj) throws IOException {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream o = new ObjectOutputStream(b);
		o.writeObject(obj);
		return b.toByteArray();
	}

	public AuditData extractAuditData(String jsonData) {
		AuditData auditData = new AuditData();

		Object obj = JSONValue.parse(jsonData);

		JSONObject jObj = (JSONObject) obj;
		if (jObj.get("process_audits") != null) {
			JSONArray jsonArray = (JSONArray) jObj.get("process_audits");
			auditData.setProcessAuditData(extractListProcessData(jsonArray));
		}
		if (jObj.get("task_audits") != null) {
			JSONArray jsonArray = (JSONArray) jObj.get("task_audits");
			auditData.setTaskAuditData(extractListTaskData(jsonArray));
		}
		if (jObj.get("user_audits") != null) {
			JSONArray jsonArray = (JSONArray) jObj.get("user_audits");
			auditData.setUserAuditData(extractListUserData(jsonArray));
		}
		return auditData;
	}

	public List<ProcessAuditData> extractListProcessData(JSONArray jsonArray) {
		List<ProcessAuditData> listProcessAuditData = new ArrayList<ProcessAuditData>();
		for (int i = 0; i < jsonArray.size(); i++) {
			ProcessAuditData processAuditData = new ProcessAuditData();
			JSONObject jObj = (JSONObject) jsonArray.get(i);
			processAuditData.setProcessId(jObj.get("process_id").toString());
			processAuditData.setAuditId(jObj.get("audit_id").toString());
			processAuditData.setProcessName(jObj.get("process_name").toString());
			processAuditData.setStatus(jObj.get("status").toString());
			processAuditData.setStartAt(jObj.get("start_time").toString());
			processAuditData.setDuration(jObj.get("duration").toString());
			processAuditData.setErrorMsg(jObj.get("error_message").toString());
			processAuditData.setAuditTime(jObj.get("end_time").toString());

			listProcessAuditData.add(processAuditData);
		}
		return listProcessAuditData;
	}

	public List<TaskAuditData> extractListTaskData(JSONArray jsonArray) {
		List<TaskAuditData> listTaskAuditData = new ArrayList<TaskAuditData>();
		for (int i = 0; i < jsonArray.size(); i++) {
			TaskAuditData taskAuditData = new TaskAuditData();
			JSONObject jObj = (JSONObject) jsonArray.get(i);
			taskAuditData.setTaskId(jObj.get("task_id").toString());
			taskAuditData.setProcessId(jObj.get("process_id").toString());
			taskAuditData.setStatus(jObj.get("status").toString());
			taskAuditData.setTaskName(jObj.get("task_name").toString());
			taskAuditData.setStartAt(jObj.get("start_time").toString());
			taskAuditData.setDuration(jObj.get("duration").toString());
			taskAuditData.setException(jObj.get("error_message").toString());
			taskAuditData.setAuditTime(jObj.get("end_time").toString());

			listTaskAuditData.add(taskAuditData);
		}
		return listTaskAuditData;
	}

	public List<UserAuditData> extractListUserData(JSONArray jsonArray) {
		List<UserAuditData> listUserAuditData = new ArrayList<UserAuditData>();
		for (int i = 0; i < jsonArray.size(); i++) {
			UserAuditData userAuditData = new UserAuditData();
			JSONObject jObj = (JSONObject) jsonArray.get(i);
			userAuditData.setAuditId(jObj.get("audit_id").toString());
			userAuditData.setTransactionId(jObj.get("transaction_id").toString());
			userAuditData.setStatus(jObj.get("status").toString());
			userAuditData.setDocId(jObj.get("document_id").toString());
			userAuditData.setOldVersion(jObj.get("old_version").toString());
			userAuditData.setNewVersion(jObj.get("new_version").toString());
			userAuditData.setDocType(jObj.get("doc_type").toString());
			userAuditData.setAction(jObj.get("action").toString());
			userAuditData.setActionBy(jObj.get("action_by").toString());
			userAuditData.setStartAt(jObj.get("start_time").toString());
			userAuditData.setDuration(jObj.get("duration").toString());
			userAuditData.setRequest(jObj.get("request").toString());
			userAuditData.setResponse(jObj.get("response").toString());
			userAuditData.setContextPath(jObj.get("context_path").toString());
			userAuditData.setErrorMsg(jObj.get("error_message").toString());
			userAuditData.setHitCache(jObj.get("hit_cache").toString());
			userAuditData.setPlatform(jObj.get("platform").toString());
			userAuditData.setDevice(jObj.get("device").toString());
			userAuditData.setAuditTime(jObj.get("end_time").toString());

			listUserAuditData.add(userAuditData);
		}
		return listUserAuditData;
	}

	public AppNotification extractAppNotificationsData(String deviceToken,
			String userId, String appDeviceId) {
		AppNotification appNotification = new AppNotification();
		appNotification.setDeviceToken(deviceToken);
		appNotification.setUserId(userId);
		appNotification.setAppDeviceId(appDeviceId);

		return appNotification;
	}

	public Device extractDevicesData(String deviceInfo) throws Exception {
		Device device = new Device();
		Object obj = JSONValue.parse(deviceInfo);
		if (obj instanceof JSONObject) {
			JSONObject jObj = (JSONObject) obj;
			device.setDeviceId(jObj.get("device_id").toString());
			device.setPlatform(jObj.get("platform").toString());
			device.setOsVersion(jObj.get("os_version").toString());
			device.setModel(jObj.get("model").toString());
			device.setManufacturer(jObj.get("manufacturer").toString());
		} else {
			throw new Exception("Input is not a valid Json object!");
		}
		return device;
	}

	public AppDevice extractAppDevicesData(String appDeviceInfo)
			throws Exception {
		AppDevice appDevice = new AppDevice();
		Object obj = JSONValue.parse(appDeviceInfo);
		if (obj instanceof JSONObject) {
			JSONObject jObj = (JSONObject) obj;
			appDevice.setAppDeviceId(generatorHelper.getRandomUUID());
			appDevice.setDeviceId(jObj.get("device_id").toString());
			appDevice.setIosAppBundleId(jObj.get("ios_app_bundle_id").toString());
			appDevice.setGooglePackageName(jObj.get("google_package_name").toString());
			appDevice.setAppVersion(jObj.get("app_version").toString());
		} else {
			throw new Exception("Input is not a valid Json object!");
		}
		return appDevice;
	}

}
