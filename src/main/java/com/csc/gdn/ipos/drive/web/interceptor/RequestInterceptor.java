package com.csc.gdn.ipos.drive.web.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.csc.gdn.ipos.drive.api.DriveConstants;
import com.csc.gdn.ipos.drive.api.impl.helper.SecurityHelper;




@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {
	@Autowired
	protected SecurityHelper securityHelper;
	final static Logger logger = Logger.getLogger(RequestInterceptor.class);
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		boolean status = false;
//		try {
//			String data;
//			data = request.getHeader(DriveConstants.HEADER_SESSION_TOKEN);
//			String url = request.getRequestURI().substring(request.getContextPath().length());
//			data += url.substring(1, url.length());
//			if (validateHMAC(request.getHeader(DriveConstants.HEADER_CSRF_TOKEN), data)) {
//				status = true;
//			} else {
//				response.setStatus(401);
//				status = false;
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		try {
			String ipAddress = request.getHeader("X-FORWARDED-FOR");  
			if (ipAddress == null) {  
				ipAddress = request.getRemoteAddr();  
			}
			if(request.getParameterMap().get("file") != null) {
				logger.info("[IP Address] " + ipAddress + " - [File Name] " + request.getParameterMap().get("file") + " - [Url] " + request.getRequestURI());
				System.out.println("[IP Address] " + ipAddress + " - [File Name] " + request.getParameterMap().get("file") + " - [Url] " + request.getRequestURI());
			} else {
				logger.info("[IP Address]" + ipAddress + " - [Url] " + request.getRequestURI());
				System.out.println("[IP Address]" + ipAddress + " - [Url] " + request.getRequestURI());
			}
			
			String data;
			String token = request.getHeader(DriveConstants.HEADER_SESSION_TOKEN);
			data = token.substring(Math.max(token.length() - 4, 0));
			String url = request.getRequestURI().substring(request.getContextPath().length());
			data += url.substring(Math.max(url.length() - 4, 0));
			if (validateHMAC(request.getHeader(DriveConstants.HEADER_CSRF_TOKEN), data)) {
				status = true;
			} else {
				response.setStatus(401);
				status = false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			response.setStatus(401);
		}
			
		return status;
	}

	private boolean validateHMAC(String hmac, String data) {
		if (hmac.equals(securityHelper.encryptByStringKey(data))) {
			return true;
		}
		return false;
	}
}
