package com.csc.gdn.ipos.drive.web.bsi;

import com.csc.gdn.ipos.core.service.message.MessageHelper;
import com.csc.gdn.ipos.drive.storage.controller.IStorageController;

public abstract class AbstractBaseBusinessServiceInterface implements IBaseBusinessServiceInterface {
	
	protected IStorageController driveStorageManager;
    protected MessageHelper messageHelper;
    
	public MessageHelper getMessageHelper() {
		return messageHelper;
	}

	public void setMessageHelper(MessageHelper messageHelper) {
		this.messageHelper = messageHelper;
	}

	public IStorageController getDriveStorageManager() {
		return driveStorageManager;
	}

	public void setDriveStorageManager(IStorageController driveStorageManager) {
		this.driveStorageManager = driveStorageManager;
	}

    
    
}
