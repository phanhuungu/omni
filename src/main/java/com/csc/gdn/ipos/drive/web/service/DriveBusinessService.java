package com.csc.gdn.ipos.drive.web.service;
import java.util.HashMap;
import java.util.Map;

import com.csc.gdn.ipos.drive.web.model.InternalDriveSession;


public class DriveBusinessService {

		private Map<String, InternalDriveSession> internalSessionMap = new HashMap<String, InternalDriveSession>();
		
		
		public DriveBusinessService(){
			
		}
		
		public String addInternalSession(String authToken, InternalDriveSession internalSession){			
			internalSessionMap.put(authToken, internalSession);			
			return "success";
		}

		public String removeInternalSession(String authToken){			
			internalSessionMap.remove(authToken);			
			return "success";
		}
		
		public InternalDriveSession getInternalSession(String authToken){
			return internalSessionMap.get(authToken);
		}
}
