package com.csc.gdn.ipos.drive.web.bsi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.csc.gdn.ipos.core.common.GeneratorHelper;
import com.csc.gdn.ipos.core.exception.ServiceProcessException;
import com.csc.gdn.ipos.core.key.mapping.InputKey;
import com.csc.gdn.ipos.core.profile.AbstractProfile;
import com.csc.gdn.ipos.core.profile.DriveStorageProfile;
import com.csc.gdn.ipos.core.profile.ProfileInput;
import com.csc.gdn.ipos.core.profile.RequestProfile;
import com.csc.gdn.ipos.core.service.message.Message;
import com.csc.gdn.ipos.drive.api.DriveKeys;
import com.csc.gdn.ipos.drive.api.DriveMessages;
import com.csc.gdn.ipos.drive.api.model.DriveRequest;
import com.csc.gdn.ipos.drive.api.model.OldDriveResponse;
import com.csc.gdn.ipos.drive.api.model.DriveResponse;
import com.csc.gdn.ipos.drive.storage.key.mapping.DriveStorageInputKey;
import com.csc.gdn.ipos.drive.storage.model.DriveStorageDataSource;
import com.csc.gdn.ipos.drive.web.helper.DriveHelper;
import com.csc.gdn.ipos.drive.web.model.InternalDriveSession;


public class AbstractBusinessServiceInterface extends BaseBusinessServiceInterfaceImpl {
	
    protected GeneratorHelper generatorHelper;
	
    protected DriveHelper driveHelper;
    
	protected Map<String, InternalDriveSession> driveSessionMap;

	@Autowired
	private DriveStorageDataSource driveStorageDataSource;
	
	public Map<String, InternalDriveSession> getDriveSessionMap() {
		return driveSessionMap;
	}

	public void setDriveSessionMap(Map<String, InternalDriveSession> driveSessionMap) {
		this.driveSessionMap = driveSessionMap;
	}

	public GeneratorHelper getGeneratorHelper() {
		return generatorHelper;
	}

	public void setGeneratorHelper(GeneratorHelper generatorHelper) {
		this.generatorHelper = generatorHelper;
	}	

	public DriveHelper getDriveHelper() {
		return driveHelper;
	}

	public void setDriveHelper(DriveHelper driveHelper) {
		this.driveHelper = driveHelper;
	}

	public OldDriveResponse getDriveSession(RequestProfile requestProfile, String username, String password) throws ServiceProcessException {		
		// TODO Auto-generated method stub
		
		OldDriveResponse driveResponse = new OldDriveResponse();
		if (canLogin(username, password)){
			InternalDriveSession interResultCheck = null;
			interResultCheck = checkInternalDriveSession(username, password, InternalDriveSession.IOS);
			if (interResultCheck != null){
				driveResponse.setResult(interResultCheck.getDriveAuthToken());
				return driveResponse;
			}
			
			InternalDriveSession internalDriveSession = createNewInternalSession(username, password, InternalDriveSession.IOS);
			driveResponse.setResult(internalDriveSession.getDriveAuthToken());
		} else {
			driveResponse.setErrorMsg("Invalid username/password");
			driveResponse.setResult(null);
		}
		
		return driveResponse;
	}	
	
	public OldDriveResponse getDriveSessionForRuntime(RequestProfile requestProfile, String username, String password) throws ServiceProcessException {
		OldDriveResponse driveResponse = new OldDriveResponse();
		if (canLogin(username, password)){
			InternalDriveSession interResultCheck = null;
			interResultCheck = checkInternalDriveSession(username, password, InternalDriveSession.RUNTIME);
			if (interResultCheck != null){
				driveResponse.setResult(interResultCheck.getDriveAuthToken());
				driveResponse.setCounter(interResultCheck.getCounter());
				return driveResponse;
			}
			
			InternalDriveSession internalDriveSession = createNewInternalSession(username, password, InternalDriveSession.RUNTIME);
			driveResponse.setResult(internalDriveSession.getDriveAuthToken());
			
			driveResponse.setResult(internalDriveSession.getDriveAuthToken());
			driveResponse.setCounter(internalDriveSession.getCounter());
		} else {
			driveResponse.setErrorMsg("Invalid username/password");
			driveResponse.setResult(null);
		}
		
		return driveResponse;
	}
	
	private InternalDriveSession checkInternalDriveSession(String username, String password, String platform){
		long currentTime = System.currentTimeMillis();
		for (Map.Entry<String, InternalDriveSession> entry : driveSessionMap.entrySet()){
			InternalDriveSession inDriveSession = null;
			inDriveSession = entry.getValue();
			if (inDriveSession.getPlatform().equals(platform)
					&& (inDriveSession.getCounter()>0
						|| currentTime - inDriveSession.getLastActiveTimestamp() < driveStorageDataSource.getSessionTimeout())
					&& inDriveSession.getUsername().equals(username) 
					&& inDriveSession.getPassword().equals(password)){
				return inDriveSession;
			}
		}
		return null;
	}
	
	private InternalDriveSession createNewInternalSession (String username, String password, String platform){
		String driveSessionAuthToken = null;
		driveSessionAuthToken = createDriveSessionAuthToken();
		InternalDriveSession internalDriveSession = new InternalDriveSession();
		internalDriveSession.setDriveAuthToken(driveSessionAuthToken);
		internalDriveSession.setPassword(password);
		internalDriveSession.setUsername(username);
		internalDriveSession.setPlatform(platform);
		if(InternalDriveSession.RUNTIME.equals(platform)) internalDriveSession.setCounter(driveStorageDataSource.getSessionCounter());
		else if(InternalDriveSession.IOS.equals(platform)) internalDriveSession.setLastActiveTimestamp(System.currentTimeMillis());
		driveSessionMap.put(driveSessionAuthToken, internalDriveSession);
		return internalDriveSession;
	}
	
	private String createDriveSessionAuthToken() {
		// TODO Auto-generated method stub		
		return this.getGeneratorHelper().getRandomUUID();
	}


	private boolean canLogin( String username, String password) throws ServiceProcessException {
		// TODO Auto-generated method stub
		
		RequestProfile requestProfile = driveHelper.getEmptyRequestProfile();
		DriveRequest driveRequest = new DriveRequest();
		// add common values to drive request
		driveRequest.setUserName(username);
		driveRequest.setPassword(password);
		
		
		Message request = messageHelper.newMessage((RequestProfile)requestProfile, getDriveStorageProfile("checkAccountCanLogin", driveRequest));					
		List<?> results = messageHelper.getResults(driveStorageManager.process(request));
		
		OldDriveResponse driveResponse = (OldDriveResponse)(results.get(0));		
		String result = (String) driveResponse.getResult();
		
		if (result!=null && "true".equalsIgnoreCase(result.toLowerCase())){
			return true;
		}		
		return false;
	}

	public InternalDriveSession getInternalSession(String authToken){
		InternalDriveSession internalDriveSession = driveSessionMap.get(authToken);
		return internalDriveSession;
	}
	
	public DriveStorageProfile getDriveStorageProfile(String serviceName, DriveRequest driveRequest){
		Map<InputKey, Object> input = new HashMap<InputKey, Object>();
		input.put(DriveStorageInputKey.DRIVE_REQUEST, driveRequest);
		
		ProfileInput profileInput = new ProfileInput(input);
		
		DriveStorageProfile driveStorageProfile = new DriveStorageProfile(serviceName, profileInput);

		return driveStorageProfile;
	}

	public OldDriveResponse doDriveService(String authToken, String serviceName, AbstractProfile requestProfile, DriveRequest driveRequest) throws ServiceProcessException{
		OldDriveResponse driveResponse = new OldDriveResponse();
		InternalDriveSession internalDriveSession = getInternalSession(authToken);
		if (internalDriveSession == null) {
			driveResponse.setErrorMsg("Invalid authentication token");
			driveResponse.setResult(null);
		} else {
			if (internalDriveSession.getPlatform().equals(InternalDriveSession.IOS)){
				long currentTime = System.currentTimeMillis();
				if (currentTime - internalDriveSession.getLastActiveTimestamp() < driveStorageDataSource.getSessionTimeout()){
					driveSessionMap.get(authToken).setLastActiveTimestamp(currentTime);
				}else{
					driveSessionMap.remove(authToken);
					driveResponse.setErrorMsg("auth_token_time_out");
					return driveResponse;
				}
			}else{
				if (internalDriveSession.getPlatform().equals(InternalDriveSession.RUNTIME)){
					if (internalDriveSession.getCounter()<1){
						driveSessionMap.remove(authToken);
						driveResponse.setErrorMsg("auth_token_need_renew");
						return driveResponse;
					}else {
						internalDriveSession.setCounter(internalDriveSession.getCounter()-1);
					}
				}
			}
			
			// add common values to drive request
			driveRequest.setUserName(internalDriveSession.getUsername());
			driveRequest.setPassword(internalDriveSession.getPassword());
			
			Message request = messageHelper.newMessage((RequestProfile)requestProfile, getDriveStorageProfile(serviceName, driveRequest));					
			List<?> results = messageHelper.getResults(driveStorageManager.process(request));
			driveResponse = (OldDriveResponse)(results.get(0));
			if (internalDriveSession.getPlatform().equals(InternalDriveSession.RUNTIME)){
				if (internalDriveSession.getCounter()<1){
					driveSessionMap.remove(internalDriveSession.getDriveAuthToken());
				}
				driveResponse.setCounter(internalDriveSession.getCounter());
				driveResponse.setAuth_token(authToken);
			}
		}
		return driveResponse;

	}
	
	public OldDriveResponse doMySQLService(String serviceName, AbstractProfile requestProfile, DriveRequest driveRequest) throws ServiceProcessException{
		OldDriveResponse driveResponse = new OldDriveResponse();
		Message request = messageHelper.newMessage((RequestProfile)requestProfile, getDriveStorageProfile(serviceName, driveRequest));					
		List<?> results = messageHelper.getResults(driveStorageManager.process(request));
		driveResponse = (OldDriveResponse)(results.get(0));
		
		return driveResponse;

	}
	
	public DriveResponse doNewMySQLService(String serviceName, AbstractProfile requestProfile, DriveRequest driveRequest) throws ServiceProcessException{
		DriveResponse driveResponse = new DriveResponse();
		Message request = messageHelper.newMessage((RequestProfile)requestProfile, getDriveStorageProfile(serviceName, driveRequest));					
		List<?> results = messageHelper.getResults(driveStorageManager.process(request));
		driveResponse = (DriveResponse)(results.get(0));
		
		return driveResponse;
		
	}
	
	public DriveResponse doDriveStorage(String authToken, String serviceName, AbstractProfile requestProfile, DriveRequest driveRequest) throws ServiceProcessException{
		DriveResponse driveResponse = new DriveResponse();
		InternalDriveSession internalDriveSession = getInternalSession(authToken);
		if (internalDriveSession == null) {
			driveResponse.setError_code(1);
			driveResponse.setError_message(DriveMessages.ERROR_MSG_INVALID_TOKEN);
		} else {
			if (internalDriveSession.getPlatform().equals(InternalDriveSession.IOS)){
				long currentTime = System.currentTimeMillis();
				if (currentTime - internalDriveSession.getLastActiveTimestamp() < driveStorageDataSource.getSessionTimeout()){
					driveSessionMap.get(authToken).setLastActiveTimestamp(currentTime);
				}else{
					driveSessionMap.remove(authToken);
					driveResponse.setError_code(1);
					driveResponse.setError_message(DriveMessages.ERROR_MSG_TOKEN_TIMEOUT);
					return driveResponse;
				}
			}else{
				if (internalDriveSession.getPlatform().equals(InternalDriveSession.RUNTIME)){
					if (internalDriveSession.getCounter()<1){
						driveSessionMap.remove(authToken);
						driveResponse.setError_code(1);
						driveResponse.setError_message(DriveMessages.ERROR_MSG_TOKEN_NEED_RENEW);
						return driveResponse;
					}else {
						internalDriveSession.setCounter(internalDriveSession.getCounter()-1);
					}
				}
			}
			
			driveRequest.setUserName(internalDriveSession.getUsername());
			driveRequest.setPassword(internalDriveSession.getPassword());
			
			Message request = messageHelper.newMessage((RequestProfile)requestProfile, getDriveStorageProfile(serviceName, driveRequest));					
			List<?> results = messageHelper.getResults(driveStorageManager.process(request));
			driveResponse = (DriveResponse)(results.get(0));
			if (internalDriveSession.getPlatform().equals(InternalDriveSession.RUNTIME)){
				if (internalDriveSession.getCounter()<1){
					driveSessionMap.remove(internalDriveSession.getDriveAuthToken());
				}
				if(driveResponse.getData()!=null){
					driveResponse.getData().put(DriveKeys.COUNTER, internalDriveSession.getCounter());
					driveResponse.getData().put(DriveKeys.AUTH_TOKEN, authToken);
				}else{
					Map<DriveKeys, Object> dataObject = new HashMap<DriveKeys, Object>();
					dataObject.put(DriveKeys.COUNTER, internalDriveSession.getCounter());
					dataObject.put(DriveKeys.AUTH_TOKEN, authToken);
					driveResponse.setData(dataObject);
				}
	
			}
		}
		return driveResponse;
	}
}
