package com.csc.gdn.ipos.drive.web.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import com.csc.gdn.ipos.drive.api.model.OldDriveResponse;


public class RetrieveFoldersServiceUnitTest extends AbstractDriveTest{

	@Test
	public void testRetrieveFolders(){
		MvcResult mvcResult;
		try {
			mvcResult = this.mockMvc.perform(post("/drive/folders?repositoryId=10184&folderId=0&encryptedUserName=test@liferay.com&encryptPassword=test").accept(MediaType.APPLICATION_JSON)).andReturn();
			MockHttpServletResponse response = mvcResult.getResponse();
			int status =  response.getStatus();
			Assert.assertEquals(200, status);
			String content = mvcResult.getResponse().getContentAsString();
			OldDriveResponse result = jsonObjectMapper.readValue(content, OldDriveResponse.class);
			Assert.assertTrue(result != null);
			System.out.println("Error Message: " + result.getErrorMsg());
			System.out.println("Result: " + result.getResult());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
