package com.csc.gdn.ipos.drive.web.test;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.csc.gdn.ipos.core.exception.ServiceProcessException;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("/META-INF/test/ipos-drive-web-context-test.xml")
public class AbstractDriveTest {
	@Autowired
	protected WebApplicationContext wac;
    protected MockMvc mockMvc;
    protected ObjectMapper jsonObjectMapper;
    
    @Before
    public void setup() throws ServiceProcessException {
    	//MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        this.jsonObjectMapper = new ObjectMapper();
    }
}
