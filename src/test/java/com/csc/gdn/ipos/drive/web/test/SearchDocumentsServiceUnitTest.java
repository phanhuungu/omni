package com.csc.gdn.ipos.drive.web.test;

import javax.annotation.Resource;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.csc.gdn.ipos.core.profile.RequestProfile;
import com.csc.gdn.ipos.core.service.controller.IServiceController;
import com.csc.gdn.ipos.core.service.message.Message;
import com.csc.gdn.ipos.core.service.message.MessageHelper;
import com.csc.gdn.ipos.drive.api.model.DriveRequest;
import com.google.gson.Gson;
import com.liferay.portal.kernel.search.SearchContext;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/META-INF/test/ipos-drive-web-context-test.xml")
public class SearchDocumentsServiceUnitTest {
	@Resource
	protected MessageHelper messageHelper;

	@Resource
	protected IServiceController driveManager;

	@Test
	public void testSearchDocument() throws Exception {
		DriveRequest driveRequest = new DriveRequest();
		driveRequest.setUserName("test@liferay.com");
		driveRequest.setPassword("test");
		driveRequest.setRepositoryId("10184");

		SearchContext searchContext = new SearchContext();
		searchContext.setCompanyId(10157);
		long[] groupIds = { 10184 };
		searchContext.setGroupIds(groupIds);
		searchContext.setKeywords("prospect");
		
		String context = new Gson().toJson(searchContext);
		System.out.println(context);
		driveRequest.setRequestUri(context);
		String pojo = new Gson().toJson(driveRequest);
		System.out.println(pojo);
		ObjectMapper mapper = new ObjectMapper();
		try {
			byte[] bytes = mapper.writeValueAsBytes(driveRequest);
			driveRequest.setBytes(bytes);
		} catch (Exception e) {
			e.printStackTrace();
		}

/*		RequestProfile requestProfile = new RequestProfile();
		requestProfile.setServiceName("searchDocuments");

		Message request = messageHelper.newMessage(requestProfile);
		messageHelper.putParam2(request, driveRequest);

		System.out.println(driveManager.process(request));
*/
	}

}
